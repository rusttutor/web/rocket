#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;

#[get("/")]
fn index() -> &'static str {
    "Hello rocket!"
}

fn main() {
    println!("Launching a rocket...");
    rocket::ignite().mount("/", routes![index]).launch();
}
